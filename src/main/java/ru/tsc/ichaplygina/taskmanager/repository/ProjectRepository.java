package ru.tsc.ichaplygina.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

    List<Project> findAllByUserId(final String userId);

    Project findByUserIdAndId(final String userId, final String id);

    void deleteAllByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

}
