package ru.tsc.ichaplygina.taskmanager.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, String> {

    List<Task> findAllByUserId(final String userId);

    Task findByUserIdAndId(final String userId, final String id);

    void deleteAllByUserId(final String userId);

    void deleteByUserIdAndId(final String userId, final String id);

}
