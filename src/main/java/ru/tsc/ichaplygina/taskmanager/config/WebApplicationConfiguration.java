package ru.tsc.ichaplygina.taskmanager.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.tsc.ichaplygina.taskmanager.api.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@EnableWebSecurity
@EnableGlobalAuthentication
@ComponentScan("ru.tsc.ichaplygina.taskmanager")
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
public class WebApplicationConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer, WebApplicationInitializer {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Override
    public void addViewControllers(@NotNull final ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Bean
    public Endpoint projectEndpointRegistry(
            final ProjectEndpoint projectEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint =
                new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(
            final TaskEndpoint taskEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectsEndpointRegistry(
            final ProjectsEndpoint projectsEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, projectsEndpoint);
        endpoint.publish("/ProjectsEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint tasksEndpointRegistry(
            final TasksEndpoint tasksEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, tasksEndpoint);
        endpoint.publish("/TasksEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpointRegistry(
            final AuthEndpoint authEndpoint, SpringBus cxf
    ) {
        @NotNull final Endpoint endpoint = new EndpointImpl(cxf, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Override
    public void onStartup(@NotNull final ServletContext servletContext) throws ServletException {
        @NotNull final CXFServlet cxfServlet = new CXFServlet();
        @NotNull final ServletRegistration.Dynamic dynamic = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamic.addMapping("/ws/*");
        dynamic.setLoadOnStartup(1);
    }

    @Override
    public void configure(@NotNull final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(@NotNull final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/auth/**", "/ws/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/")
                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")
                .and()
                .csrf().disable();
    }

}
