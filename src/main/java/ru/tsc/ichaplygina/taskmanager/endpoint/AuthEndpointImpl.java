package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.tsc.ichaplygina.taskmanager.api.AuthEndpoint;
import ru.tsc.ichaplygina.taskmanager.model.Result;
import ru.tsc.ichaplygina.taskmanager.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/auth")
@WebService(endpointInterface = "ru.tsc.ichaplygina.taskmanager.api.AuthEndpoint")
public class AuthEndpointImpl implements AuthEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @WebMethod
    @GetMapping(value = "/login", produces = "application/json")
    public Result login(@RequestParam("username") @WebParam(name = "username") final String username,
                        @RequestParam("password") @WebParam(name = "password") final String password) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            return new Result(e);
        }
    }

    @WebMethod
    @GetMapping(value = "/logout", produces = "application/json")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
