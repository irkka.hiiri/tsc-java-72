package ru.tsc.ichaplygina.taskmanager.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.ichaplygina.taskmanager.config.DatabaseConfiguration;
import ru.tsc.ichaplygina.taskmanager.marker.UnitCategory;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.repository.TaskRepository;
import ru.tsc.ichaplygina.taskmanager.util.UserUtil;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class TaskRepositoryTest {

    @NotNull
    private final Task task1 = new Task("Test Task 1");

    @NotNull
    private final Task task2 = new Task("Test Task 2");

    @NotNull
    private final Task task3 = new Task("Test Task 3");

    @NotNull
    private final Task task4 = new Task("Test Task 4");

    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        task1.setUserId(UserUtil.getUserId());
        task2.setUserId(UserUtil.getUserId());
        task3.setUserId(UserUtil.getUserId());
        task4.setUserId(UserUtil.getUserId());
        taskRepository.save(task1);
        taskRepository.save(task2);
    }

    @After
    public void clean() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findByUserIdAndIdTest() {
        Assert.assertNotNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteAllByUserIdTest() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskRepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void deleteByUserIdAndIdTest() {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task1.getId());
        Assert.assertNull(taskRepository.findByUserIdAndId(UserUtil.getUserId(), task1.getId()));
    }

}
