package ru.tsc.ichaplygina.taskmanager.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.tsc.ichaplygina.taskmanager.config.DatabaseConfiguration;
import ru.tsc.ichaplygina.taskmanager.config.WebApplicationConfiguration;
import ru.tsc.ichaplygina.taskmanager.marker.UnitCategory;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, DatabaseConfiguration.class})
public class ProjectEndpointTest {

    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project/";

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/projects/";

    @NotNull
    private final Project project1 = new Project("Test Project 1");

    @NotNull
    private final Project project2 = new Project("Test Project 2");

    @NotNull
    private final Project project3 = new Project("Test Project 3");

    @NotNull
    private final Project project4 = new Project("Test Project 4");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("user", "user");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        add(project1);
        add(project2);
    }

    @After
    @SneakyThrows
    public void clean() {
        mockMvc.perform(MockMvcRequestBuilders.delete(PROJECTS_URL + "removeAll")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void add(@NotNull final Project project) {
        @NotNull String url = PROJECT_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void addAll(@NotNull final List<Project> projects) {
        @NotNull String url = PROJECTS_URL + "add";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Category(UnitCategory.class)
    public void testAdd() {
        add(project3);
        @Nullable final Project project = findById(project1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(project1.getId(), project.getId());

    }

    @Test
    @Category(UnitCategory.class)
    public void testAddAll() {
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(project3);
        projects.add(project4);
        addAll(projects);
        @Nullable final Project project = findById(project1.getId());
        Assert.assertNotNull(findById(project3.getId()));
        Assert.assertNotNull(findById(project4.getId()));
    }

    @SneakyThrows
    private Project findById(@NotNull final String id) {
        @NotNull String url = PROJECT_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Project.class);
    }

    @Test
    @Category(UnitCategory.class)
    public void findByIdTest() {
        Assert.assertNotNull(findById(project1.getId()));
    }

    @SneakyThrows
    private List<Project> findAll() {
        @NotNull final String url = PROJECTS_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Project[].class));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void findAllTest() {
        Assert.assertEquals(2, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeById() {
        @NotNull String url = PROJECT_URL + "removeById/" + project1.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(project1.getId()));
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeList() {
        @NotNull String url = PROJECTS_URL + "remove";
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(project1);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(1, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void removeAll() {
        @NotNull String url = PROJECTS_URL + "removeAll";
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertEquals(0, findAll().size());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void save() {
        @NotNull final String url = PROJECT_URL + "save/";
        @NotNull final Project project = project1;
        project.setName("New Name");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        this.mockMvc.perform(MockMvcRequestBuilders.put(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final Project newProject = findById(project.getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals("New Name", newProject.getName());
    }

    @Test
    @SneakyThrows
    @Category(UnitCategory.class)
    public void saveList() {
        @NotNull final String url = PROJECTS_URL + "save/";
        @NotNull final List<Project> projects = new ArrayList<>();
        projects.add(project1);
        projects.get(0).setName("New Name");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        this.mockMvc.perform(MockMvcRequestBuilders.put(url)
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final Project newProject = findById(projects.get(0).getId());
        Assert.assertNotNull(newProject);
        Assert.assertEquals("New Name", newProject.getName());
    }

}
